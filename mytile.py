#!/usr/bin/python

import logging
import sys
import math

from xdo import Xdo
from systemd.journal import JournalHandler

xdo = Xdo()

log = logging.getLogger('mytile')
log.addHandler(JournalHandler())
log.setLevel(logging.INFO)

marg_ratio = 0.7

opt = 'a'
tw = 1920
th = 1080

if len(sys.argv) == 4:
    opt = sys.argv[1]
    tw = int(sys.argv[2])
    th = int(sys.argv[3])

log.info("Mode [%s] on size [%d x %d]" % (opt, tw, th))

transforms = {
    'a': {
        'x': lambda w, h: 0,
        'y': lambda w, h: 0,
        'w': lambda w, h: math.floor(w*marg_ratio),
        'h': lambda w, h: h,
        '_post': lambda w, h: {}
    },

    'b': {
        'x': lambda w, h: math.ceil(w*marg_ratio),
        'y': lambda w, h: 0,
        'w': lambda w, h: math.ceil(w*(1-marg_ratio)),
        'h': lambda w, h: math.floor(h*0.5),
        '_post': lambda w, h: {}
    },

    'c': {
        'x': lambda w, h: math.ceil(w*marg_ratio),
        'y': lambda w, h: math.ceil(h*0.5),
        'w': lambda w, h: math.ceil(w*(1-marg_ratio)),
        'h': lambda w, h: math.floor(h*0.5),
        '_post': lambda w, h: {}
    },

    's': {
        'x': lambda w, h: math.floor(w/3),
        'y': lambda w, h: math.floor(h/3),
        'w': lambda w, h: math.floor(w/3),
        'h': lambda w, h: math.floor(h/3)
    }
}

if len(sys.argv) == 3:
    opt = sys.argv[1]
    tw = int(sys.argv[2])
    th = int(sys.argv[3])

current_winid = xdo.get_focused_window_sane()

x = transforms[opt]['x'](tw, th)
y = transforms[opt]['y'](tw, th)
w = transforms[opt]['w'](tw, th)
h = transforms[opt]['h'](tw, th)

log.info("Setting coords: (%d, %d) [%d x %d]" % (x, y, w, h))
xdo.move_window(current_winid, x, y)
xdo.set_window_size(current_winid, w, h)

